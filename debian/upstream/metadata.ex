# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/hueplusplus/issues
# Bug-Submit: https://github.com/<user>/hueplusplus/issues/new
# Changelog: https://github.com/<user>/hueplusplus/blob/master/CHANGES
# Documentation: https://github.com/<user>/hueplusplus/wiki
# Repository-Browse: https://github.com/<user>/hueplusplus
# Repository: https://github.com/<user>/hueplusplus.git
